package tr.com.obss.digitall.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tr.com.obss.digitall.service.TestService;

@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class TestController {

	private static final TestService service = new TestService();
	
	@GET
	public int test() {
		return service.test(); 
	}
}
