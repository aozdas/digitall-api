package tr.com.obss.digitall.controller;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tr.com.obss.digitall.service.TransactionProposalService;

@Path("/TransactionProposal")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionProposalController {

	@Resource
	private TransactionProposalService service;  
	
	@GET
    @Path("/getResponseByTxId")
    public void getResponseByTxId(long id) {
		service.getResponseByTxId(id);
    }
}
