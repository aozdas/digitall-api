package tr.com.obss.digitall.service;

import static java.lang.String.format;

import java.io.File;

import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.security.CryptoSuite;

import tr.com.obss.digitall.model.SampleStore;
import tr.com.obss.digitall.model.SampleUser;

public class TestService {

	public int test() {
		return 1;
	}

	public HFClient testClient() throws Exception {
		File tempFile = File.createTempFile("teststore", "properties");
        tempFile.deleteOnExit();

        File sampleStoreFile = new File(System.getProperty("user.home") + "/test.properties");
        if (sampleStoreFile.exists()) { //For testing start fresh
            sampleStoreFile.delete();
        }
        
        final SampleStore sampleStore = new SampleStore(sampleStoreFile);
        SampleUser someTestUSER = sampleStore.getMember("someTestUSER", "someTestORG", "mspid",
        findFileSk("src/test/fixture/sdkintegration/e2e-2Orgs/channel/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore"),
                new File("src/test/fixture/sdkintegration/e2e-2Orgs/channel/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/signcerts/Admin@org1.example.com-cert.pem"));
        someTestUSER.setMspId("testMSPID?");
        
        HFClient hfclient = HFClient.createNewInstance();
        hfclient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
        
        hfclient.setUserContext(someTestUSER);

        return hfclient;
	}
	
	static File findFileSk(String directorys) {
		File directory = new File(directorys);
		File[] matches = directory.listFiles((dir, name) -> name.endsWith("_sk"));
		if (null == matches) {
			throw new RuntimeException(
					format("Matches returned null does %s directory exist?", directory.getAbsoluteFile().getName()));
		}
		if (matches.length != 1) {
			throw new RuntimeException(format("Expected in %s only 1 sk file but found %d",
					directory.getAbsoluteFile().getName(), matches.length));
		}
		return matches[0];
	}
}
