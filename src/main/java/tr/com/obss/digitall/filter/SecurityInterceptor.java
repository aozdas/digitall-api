package tr.com.obss.digitall.filter;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.core.ResourceMethodInvoker;

public class SecurityInterceptor implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext reqCtx) throws IOException {
		ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) reqCtx
				.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
		Method method = methodInvoker.getMethod();
		// Access allowed for all
		if (!method.isAnnotationPresent(PermitAll.class)) {
			// Access denied for all
			if (method.isAnnotationPresent(DenyAll.class)) {
				reqCtx.abortWith(
						Response.status(Response.Status.FORBIDDEN).header("Content-Type", "text/html").build());
				return;
			}
		}
	}

}
