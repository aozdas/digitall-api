package tr.com.obss.digitall;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import tr.com.obss.digitall.controller.TestController;
import tr.com.obss.digitall.controller.TransactionProposalController;
import tr.com.obss.digitall.filter.SecurityInterceptor;
import tr.com.obss.digitall.service.TestService;
import tr.com.obss.digitall.service.TransactionProposalService;

@ApplicationPath("/*")
public class DigitallApp extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(TransactionProposalController.class);
		classes.add(TestController.class);
		classes.add(TestService.class);
		classes.add(TransactionProposalService.class);
		classes.add(SecurityInterceptor.class);
		return classes;
	}
}
