package tr.com.obss.digitall;

import org.jboss.resteasy.plugins.server.sun.http.SunHttpJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

public class Digitall {

	public static void main(String[] args) {
		SunHttpJaxrsServer server = new SunHttpJaxrsServer();
        ResteasyDeployment deployment = new ResteasyDeployment();
        deployment.setApplication(new DigitallApp());
        server.setDeployment(deployment);
        server.setRootResourcePath("/digitall");
        server.setPort(8080);
        server.start();
	}

}
